module bitbucket.org/spinnerweb/assets

go 1.15

require (
	bitbucket.org/spinnerweb/accounting_common v0.0.0-20201006104513-a02a0b1c28a5
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/stretchr/testify v1.4.0
	go.mongodb.org/mongo-driver v1.4.1
)
