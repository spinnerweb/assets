package main

import (
	"fmt"

	env "bitbucket.org/spinnerweb/accounting_common/env"
	asset "bitbucket.org/spinnerweb/assets/asset/crud"
	"bitbucket.org/spinnerweb/assets/server"
)

func main() {
	server := server.Configure()
	asset.Setup(server)
	server.Run(fmt.Sprintf("0.0.0.0:%s", env.GetEnv("SERVER_PORT", "4003"))) // listen and serve on 0.0.0.0:8080
}
