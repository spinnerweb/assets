package db

import (
	"context"
	"errors"
	"log"
	"time"

	"bitbucket.org/spinnerweb/assets/db"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Action string

const (
	// Buy ...
	Buy Action = "Buy"
	// Sell ...
	Sell Action = "Sell"
)

// Asset - Model of a basic asset
type Asset struct {
	ID        primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	Action    Action             `json:"action"`
	Key       string             `json:"key"`
	Name      string             `json:"name"`
	Symbol    string             `json:"symbol"`
	ISIN      string             `json:"isin"`
	Quantity  float64            `json:"quantity"`
	UnitPrice float64            `json:"unitPrice"`
	Currency  string             `json:"currency"`
	Date      time.Time          `json:"date"`
	Source    string             `json:"source"`
}

// AssetCollection ...
type AssetCollection struct {
	*mongo.Client
	*mongo.Collection
	context.Context
	context.CancelFunc
}

// GetAssetCollection ...
func GetAssetCollection() AssetCollection {
	client, ctx, cancel := db.GetConnection()
	db := client.Database(db.GetDatabaseName())
	collection := db.Collection("assets")

	return AssetCollection{
		Client:     client,
		Collection: collection,
		Context:    ctx,
		CancelFunc: cancel,
	}
}

// Disconnect ...
func (collection *AssetCollection) Disconnect() {
	collection.CancelFunc()
	collection.Client.Disconnect(collection.Context)
}

// GetAllAssets Retrives all assets from the db
func (collection *AssetCollection) GetAllAssets() ([]*Asset, error) {
	var assets []*Asset = []*Asset{}
	ctx := collection.Context

	cursor, err := collection.Collection.Find(ctx, bson.D{})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)
	err = cursor.All(ctx, &assets)
	if err != nil {
		log.Printf("Failed marshalling %v", err)
		return nil, err
	}
	return assets, nil
}

// GetAssetByID Retrives a asset by its id from the db
func (collection *AssetCollection) GetAssetByID(id string) (*Asset, error) {
	var asset *Asset
	ctx := collection.Context

	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Printf("Failed parsing id %v", err)
		return nil, err
	}
	result := collection.Collection.FindOne(ctx, bson.D{bson.E{Key: "_id", Value: objID}})
	if result == nil {
		return nil, errors.New("Could not find a Asset")
	}
	err = result.Decode(&asset)

	if err != nil {
		log.Printf("Failed marshalling %v", err)
		return nil, err
	}
	return asset, nil
}

// GetAssetByIDs Retrives assets by their ids from the db
func (collection *AssetCollection) GetAssetByIDs(ids *[]primitive.ObjectID) (*[]Asset, error) {
	ctx := collection.Context
	var assets []Asset = []Asset{}
	cursor, err := collection.Collection.Find(ctx, bson.D{bson.E{Key: "_id", Value: bson.M{"$in": ids}}})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)
	err = cursor.All(ctx, &assets)
	if err != nil {
		log.Printf("Failed marshalling %v", err)
		return nil, err
	}
	return &assets, nil

}

// GetAssetByKey Retrives a asset by its key from the db
func (collection *AssetCollection) GetAssetByKey(key string) (*Asset, error) {
	var asset *Asset
	ctx := collection.Context

	result := collection.Collection.FindOne(ctx, bson.D{bson.E{Key: "key", Value: key}})
	if result == nil {
		return nil, nil
	}
	err := result.Decode(&asset)

	if err == mongo.ErrNoDocuments {
		return nil, nil
	}

	if err != nil {
		log.Printf("Failed marshalling %v", err)
		return nil, err
	}
	return asset, nil
}

// Create creating a asset in a mongo
func (collection *AssetCollection) Create(asset *Asset) (primitive.ObjectID, error) {
	ctx := collection.Context
	asset.ID = primitive.NewObjectID()

	result, err := collection.Collection.InsertOne(ctx, asset)
	if err != nil {
		log.Printf("Could not create Asset: %v", err)
		return primitive.NilObjectID, err
	}
	oid := result.InsertedID.(primitive.ObjectID)
	return oid, nil
}

// CreateMany creating many assets in a mongo
func (collection *AssetCollection) CreateMany(assets *[]Asset) (*[]primitive.ObjectID, error) {
	ctx := collection.Context

	var ui []interface{}
	for _, t := range *assets {
		t.ID = primitive.NewObjectID()
		ui = append(ui, t)
	}

	result, err := collection.Collection.InsertMany(ctx, ui)
	if err != nil {
		log.Printf("Could not create Asset: %v", err)
		return nil, err
	}

	var oids = []primitive.ObjectID{}

	for _, id := range result.InsertedIDs {
		oids = append(oids, id.(primitive.ObjectID))
	}
	return &oids, nil
}

//Update updating an existing asset in a mongo
func (collection *AssetCollection) Update(asset *Asset) (*Asset, error) {
	ctx := collection.Context
	var updatedAsset *Asset

	update := bson.M{
		"$set": asset,
	}

	upsert := true
	after := options.After
	opt := options.FindOneAndUpdateOptions{
		Upsert:         &upsert,
		ReturnDocument: &after,
	}

	err := collection.Collection.FindOneAndUpdate(ctx, bson.M{"_id": asset.ID}, update, &opt).Decode(&updatedAsset)
	if err != nil {
		log.Printf("Could not save Asset: %v", err)
		return nil, err
	}
	return updatedAsset, nil
}

// DeleteAssetByID Deletes an asset by its id from the db
func (collection *AssetCollection) DeleteAssetByID(id string) error {
	ctx := collection.Context
	objID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Printf("Failed parsing id %v", err)
		return err
	}
	result, err := collection.Collection.DeleteOne(ctx, bson.D{bson.E{Key: "_id", Value: objID}})
	if result == nil {
		return errors.New("Could not find a Asset")
	}

	if err != nil {
		log.Printf("Failed deleting %v", err)
		return err
	}
	return nil
}
