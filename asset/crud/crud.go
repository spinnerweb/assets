package crud

import (
	"fmt"
	"log"
	"net/http"

	db "bitbucket.org/spinnerweb/assets/asset/db"
	"github.com/gin-gonic/gin"
)

func handleGetAssets(c *gin.Context) {
	collection := db.GetAssetCollection()
	defer collection.Disconnect()

	var loadedAssets, err = collection.GetAllAssets()
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, loadedAssets)
}

func handleGetAsset(c *gin.Context) {
	collection := db.GetAssetCollection()
	defer collection.Disconnect()
	id := c.Param("id")
	var loadedAsset, err = collection.GetAssetByID(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, loadedAsset)
}

func handleCreateAsset(c *gin.Context) {
	fmt.Println("Create asset")
	collection := db.GetAssetCollection()
	defer collection.Disconnect()

	var asset db.Asset
	if err := c.ShouldBindJSON(&asset); err != nil {
		fmt.Println(err)
		log.Print(err)
		c.JSON(http.StatusBadRequest, gin.H{"msg": err})
		return
	}

	missingFields := []string{}
	if asset.Action == "" {
		missingFields = append(missingFields, "action")
	}
	if asset.Name == "" {
		missingFields = append(missingFields, "name")
	}
	if asset.ISIN == "" {
		missingFields = append(missingFields, "isin")
	}
	if asset.Symbol == "" {
		missingFields = append(missingFields, "symbol")
	}
	if asset.Key == "" {
		missingFields = append(missingFields, "key")
	}

	if len(missingFields) > 0 {
		fmt.Printf("Missing fields: %v\n", missingFields)
		c.JSON(http.StatusBadRequest, gin.H{"msg": fmt.Sprintf("Missing fields: %v", missingFields)})
		return
	}

	id, err := collection.Create(&asset)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"id": id})
}

func handleUpdateAsset(c *gin.Context) {
	collection := db.GetAssetCollection()
	defer collection.Disconnect()

	var asset db.Asset
	if err := c.ShouldBindJSON(&asset); err != nil {
		log.Print(err)
		c.JSON(http.StatusBadRequest, gin.H{"msg": err})
		return
	}
	savedAsset, err := collection.Update(&asset)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": err})
		return
	}
	c.JSON(http.StatusOK, gin.H{"asset": savedAsset})
}

func handleDeleteAsset(c *gin.Context) {
	collection := db.GetAssetCollection()
	defer collection.Disconnect()
	id := c.Param("id")
	var err = collection.DeleteAssetByID(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"msg": err})
		return
	}
	c.Status(http.StatusNoContent)
}

// Setup Setup REST API
func Setup(r *gin.Engine) {
	r.GET("/assets/:id", handleGetAsset)
	r.GET("/assets", handleGetAssets)
	r.POST("/assets", handleCreateAsset)
	r.PUT("/assets", handleUpdateAsset)
	r.DELETE("/assets/:id", handleDeleteAsset)
}
