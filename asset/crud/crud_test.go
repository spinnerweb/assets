package crud

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"bitbucket.org/spinnerweb/assets/db"
	"bitbucket.org/spinnerweb/assets/server"
)

func TestGetAssets(t *testing.T) {

	server := server.Configure()
	Setup(server)
	ts := httptest.NewServer(server)
	defer ts.Close()

	resp, err := http.Get(fmt.Sprintf("%s/assets", ts.URL))

	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if resp.StatusCode != 200 {
		t.Fatalf("Expected status code 200, got %v", resp.StatusCode)
	}

	val, ok := resp.Header["Content-Type"]

	// Assert that the "content-type" header is actually set
	if !ok {
		t.Fatalf("Expected Content-Type header to be set")
	}

	// Assert that it was set as expected
	if val[0] != "application/json; charset=utf-8" {
		t.Fatalf("Expected \"application/json; charset=utf-8\", got %s", val[0])
	}

}

func TestGetAssetNotFound(t *testing.T) {
	server := server.Configure()
	Setup(server)
	ts := httptest.NewServer(server)
	defer ts.Close()

	resp, err := http.Get(fmt.Sprintf("%s/asset/%s", ts.URL, "unknown"))

	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if resp.StatusCode != 404 {
		t.Fatalf("Expected status code 404, got %v", resp.StatusCode)
	}
}

func TestGetAssetFullCRUD(t *testing.T) {
	name := "test-asset"
	id := testCreateAsset(t, name)
	testGetAsset(t, id, name)
}

func testCreateAsset(t *testing.T, name string) string {
	server := server.Configure()
	Setup(server)
	ts := httptest.NewServer(server)
	defer ts.Close()

	requestBody, _ := json.Marshal(map[string]string{
		"name": name,
	})

	resp, err := http.Post(fmt.Sprintf("%s/assets", ts.URL), "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if resp.StatusCode != 200 {
		t.Fatalf("Expected status code 200, got %v", resp.StatusCode)
	}

	val, ok := resp.Header["Content-Type"]

	// Assert that the "content-type" header is actually set
	if !ok {
		t.Fatalf("Expected Content-Type header to be set")
	}

	// Assert that it was set as expected
	if val[0] != "application/json; charset=utf-8" {
		t.Fatalf("Expected \"application/json; charset=utf-8\", got %s", val[0])
	}

	defer resp.Body.Close()

	var res db.Asset
	json.NewDecoder(resp.Body).Decode(&res)

	return res.ID.Hex()
}

func testGetAsset(t *testing.T, id string, name string) string {
	server := server.Configure()
	Setup(server)
	ts := httptest.NewServer(server)
	defer ts.Close()

	resp, err := http.Get(fmt.Sprintf("%s/assets/%s", ts.URL, id))
	if err != nil {
		t.Fatalf("Expected no error, got %v", err)
	}

	if resp.StatusCode != 200 {
		t.Fatalf("Expected status code 200, got %v", resp.StatusCode)
	}

	val, ok := resp.Header["Content-Type"]

	// Assert that the "content-type" header is actually set
	if !ok {
		t.Fatalf("Expected Content-Type header to be set")
	}

	// Assert that it was set as expected
	if val[0] != "application/json; charset=utf-8" {
		t.Fatalf("Expected \"application/json; charset=utf-8\", got %s", val[0])
	}

	defer resp.Body.Close()

	var res db.Asset
	json.NewDecoder(resp.Body).Decode(&res)

	assert.Equal(t, name, res.Name)

	return res.ID.Hex()
}
