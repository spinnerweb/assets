FROM golang:1.20.5-alpine AS build
WORKDIR /src/
COPY . /src/
RUN unset GOPATH && go build -o /bin/assets .
ENTRYPOINT  ["/bin/assets"]